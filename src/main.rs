use std::{env,collections::HashMap,sync::Arc};
use tokio::sync::Mutex;
use serde::{Deserialize, Serialize};
use reqwest::{Client};
use serde_xml_rs::from_reader;

#[derive(Debug, Serialize, Deserialize)]
struct Failure {
    message: String,
    #[serde(rename = "type")]
    errtype: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct TestCase {
    classname: String,
    name: String,
    file: String,
    time: f32,
    failure: Option<Failure>,
}

#[derive(Debug, Serialize, Deserialize)]
struct TestSuite {
    name: String,
    tests: String,
    failures: i32,
    errors: i32,
    time: f32,
    testcase: Vec<TestCase>,
}

async fn get_artifact(url: &str, api_key: String, c: &Client, times: Arc<Mutex<HashMap<String, u32>>>) 
    -> Result<(), Box<dyn std::error::Error>> 
{
    let suite: TestSuite = match c.get(url)
        .query(&[("circle-token",api_key)])
        .send()
        .await {
            Ok(f) => {
                let s = f.text().await?;
                from_reader(s.as_bytes()).unwrap()
            },
            Err(e) => {println!("Couldn't retrieve test output {:#?} at URL {:#?}", e, url); return Err(Box::new(e));},
    };
    println!("Results from {}: tests={} failures={} errors={} time={} seconds",suite.name, suite.tests, suite.failures, suite.errors, suite.time);
    for i in suite.testcase {
        let f = i.file.clone();
        let mut times = times.lock().await;
        *times.entry(f).or_insert(0) += (i.time * 1000.0) as u32;
        if i.failure.is_some() {
            let fail = i.failure.unwrap();
            println!("Test class {} failed with error type {}: {}",i.classname, fail.errtype, fail.message);
        }
    }
    Ok(())
}

#[derive(Debug, Serialize, Deserialize)]
struct Artifact {
    node_index: u32,
    url: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct Artifacts {
    items: Vec<Artifact>,
    next_page_token: Option<String>,
}

async fn get_all_artifacts(job: u32, api_key: &str, c: &Client)
    -> Result<(), Box<dyn std::error::Error>> 
{
    let url = &(String::from("https://circleci.com/api/v2/project/github/procore/procore/") + &job.to_string() + "/artifacts");
    let resp = match c.get(url)
        .query(&[("circle-token",api_key)])
        .send()
        .await?
        .json::<Artifacts>()
        .await {
            Ok(resp) => resp,
            Err(e) => {println!("Error retrieving artifacts for job {} err: {:#?}", job, e); return Err(Box::new(e));},
        };

    let times: Arc<Mutex<HashMap<String, u32>>> = Arc::new(Mutex::new(HashMap::new()));
    let mut tasks = vec![];

    for i in resp.items {
        println!("Fetching {}", i.url);
        let times = times.clone();
        let apik = String::from(api_key);
        let c2 = c.clone();
        tasks.push(tokio::spawn(async move {
             get_artifact(i.url.as_str(), apik, &c2, times).await.unwrap();
         }));
    }

    for task in tasks {
        task.await?;
    }

    let times = times.lock().await;
    let mut count_vec: Vec<(&String, &u32)> = times.iter().collect();
    count_vec.sort_by(|a, b| b.1.cmp(a.1));
    println!("Test files by time spent(ms):");
    for i in count_vec {
        println!("{} = {}", i.0, i.1)
    }
    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let api_key = env::var("CIRCLE_API_KEY").unwrap_or_else(|e| {
        panic!("could not find CIRCLE_API_KEY {}", e)
    });
    let c = reqwest::Client::new();

    get_all_artifacts(2534074, api_key.as_str(), &c).await?;
    
    Ok(())
}